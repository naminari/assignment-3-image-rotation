#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>

static void allocate_image(struct image* img, uint64_t width, uint64_t height) {
    img->width = width;
    img->height = height;
    img->data = calloc(width * height, sizeof(struct pixel));

    if (img->data == NULL) {
        fprintf(stderr, "Error: Failed to allocate memory for image.\n");
        exit(EXIT_FAILURE);
    }
}

static void determine_dimensions(struct image const* source, int angle, struct image* result) {
    if (angle % 180 != 0) {
        result->width = source->height;
        result->height = source->width;
    } else {
        result->width = source->width;
        result->height = source->height;
    }
}

static void calculate_rotation(int angle, uint64_t src_height, uint64_t src_width, uint64_t x, uint64_t y, uint64_t* dst_x, uint64_t* dst_y) {
    switch(angle) {
        case 90:
        case -270:
            *dst_x = src_height - 1 - y;
            *dst_y = x;
            break;

        case 180:
        case -180:
            *dst_x = src_width - 1 - x;
            *dst_y = src_height - 1 - y;
            break;

        case 270:
        case -90:
            *dst_x = y;
            *dst_y = src_width - 1 - x;
            break;

        default:
            *dst_x = x;
            *dst_y = y;
            break;
    }
}

static void rotate_pixels(struct image const* source, struct image* result, int angle) {
    uint64_t src_width = source->width;
    uint64_t src_height = source->height;

    for (uint64_t y = 0; y < src_height; ++y) {
        for (uint64_t x = 0; x < src_width; ++x) {
            uint64_t src_index = y * src_width + x;
            uint64_t dst_x, dst_y;

            calculate_rotation(angle, src_height, src_width, x, y, &dst_x, &dst_y);

            uint64_t dst_index = dst_y * result->width + dst_x;
            result->data[dst_index] = source->data[src_index];
        }
    }
}



struct image rotate(struct image const* source, int angle) {
    struct image result;

    determine_dimensions(source, angle, &result);

    allocate_image(&result, result.width, result.height);

    rotate_pixels(source, &result, angle);

    return result;
}
