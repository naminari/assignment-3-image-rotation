#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/status.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define TYPE 0x4D42
#define X_PPM 0
#define Y_PPM 0
#define NUM_COLORS 0
#define PLANES 1
#define DIB_SIZE 40
#define BPP 24
#define COMP 0
#define IMP_COLORS 0
#define BMP_P 4

static uint8_t get_padding(uint32_t width) {
    return (BMP_P - (width * sizeof(struct pixel)) % BMP_P) % BMP_P;
}

enum read_status from_bmp(FILE* file, struct image* img) {
    if (!file) {
        return FILE_READ_ERROR;
    }

    struct bmp_header header = {0};
    size_t elements_read = fread(&header, sizeof(struct bmp_header), 1, file);
    if (elements_read != 1) {
        fclose(file);
        return READ_INVALID_HEADER;
    }
    if (header.bfType != TYPE) {
        fclose(file);
        return READ_INVALID_HEADER;
    }

    img->width = (int) header.biWidth;
    img->height = (int) header.biHeight;
    img->data = (struct pixel*) malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        fclose(file);
        return READ_INVALID_DATA;
    }

    int new_width = (int) img->width;
    int new_height = (int) img->height;

    for (int y = new_height - 1; y >= 0; y--) {
        for (int x = 0; x < new_width; x++) {
            struct pixel p;
            fread(&p, sizeof(struct pixel), 1, file);
            img->data[y * new_width + x] = p;
        }
        for (int padding = 0; padding < get_padding(new_width); padding++) {
            fgetc(file);
        }
    }

    fclose(file);
    return READ_OK;
}

enum write_status to_bmp(FILE* file, struct image const* img) {
    if (!file) {
        return WRITE_ERROR;
    }

    int new_width = (int) img->width;
    int new_height = (int) img->height;

    struct bmp_header header = {
            .bfType = TYPE,
            .bfileSize = DIB_SIZE + (new_width * BPP + get_padding(new_width)) * new_height,
            .bfReserved = 0,
            .bOffBits = DIB_SIZE,
            .biSize = DIB_SIZE,
            .biWidth = new_width,
            .biHeight = new_height,
            .biPlanes = PLANES,
            .biBitCount = BPP,
            .biCompression = COMP,
            .biSizeImage = (new_width * BPP + get_padding(new_width)) * new_height,
            .biXPelsPerMeter = X_PPM,
            .biYPelsPerMeter = Y_PPM,
            .biClrUsed = NUM_COLORS,
            .biClrImportant = IMP_COLORS
    };

    size_t header_written = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (header_written != 1) {
        fclose(file);
        return WRITE_ERROR;
    }
    for (int y = new_height - 1; y >= 0; y--) {
        for (int x = 0; x < new_width; x++) {
            struct pixel p = img->data[y * new_width + x];
            fputc(p.b, file);
            fputc(p.g, file);
            fputc(p.r, file);
        }
        for (int padding = 0; padding < get_padding(new_width); padding++) {
            fputc(0x00, file);
        }
        if (ferror(file)) {
            fclose(file);
            return WRITE_ERROR;
        }
    }

    fclose(file);
    return WRITE_OK;
}


