#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"

#include <stdio.h>
#include <stdlib.h>

#define REQUIRED_ARGUMENTS 4
#define MIN_ANGLE (-270)
#define MAX_ANGLE 270
#define ANGLE_DIVISOR 90

#define EXIT_ERROR 1
#define EXIT_SUCCESS 0

int isValidAngle(long angle) {
    return (angle % ANGLE_DIVISOR == 0 || angle >= MIN_ANGLE || angle <= MAX_ANGLE);
}

int main(int argc, char* argv[]) {

    // Check for the required number of arguments
    if (argc != REQUIRED_ARGUMENTS) {
        fprintf(stderr, "Invalid number of arguments\n");
        return EXIT_ERROR;
    }

    FILE* source_file = fopen(argv[1], "rb");
    FILE* transformed_file = fopen(argv[2], "wb");
    long angle = strtol(argv[3], NULL, 10);

    if (!isValidAngle(angle)) {
        fprintf(stderr, "Enter a valid angle within the range of int\n");
        return EXIT_ERROR;
    }

    int int_angle = (int)angle;

    struct image source_image = { 0 };
    if (from_bmp(source_file, &source_image) != READ_OK) {
        fprintf(stderr, "Error reading the source image\n");
        free(source_image.data);
        return EXIT_ERROR;
    }

    struct image transformed_image = rotate(&source_image, int_angle);

    if (to_bmp(transformed_file, &transformed_image) != WRITE_OK) {
        fprintf(stderr, "Error writing the transformed image\n");
        free(source_image.data);
        free(transformed_image.data);
        return EXIT_ERROR;
    }

    free(source_image.data);
    free(transformed_image.data);

    return EXIT_SUCCESS;
}
