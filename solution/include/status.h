#ifndef STATUS
#define STATUS

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR= 1
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER = 1,
    FILE_READ_ERROR = 2,
    READ_INVALID_DATA = 3
};

#endif
